#include "route_planner.h"
#include <algorithm>

RoutePlanner::RoutePlanner(RouteModel &model, float start_x, float start_y, float end_x, float end_y): m_Model(model) {
    this->m_Model=model;
    start_x*=0.01;
    start_y*=0.01;
    end_x*=0.01;
    end_y*=0.01;
    std::cout<<"start point cordination is "<<start_x<<" , "<<start_y<<std::endl;
    std::cout<<"end point cordination is "<<end_x<<" , "<<end_y<<std::endl;

    this->start_node=&m_Model.FindClosestNode(start_x,start_y);
    this->end_node=&m_Model.FindClosestNode(end_x,end_y);
    
    std::cout<<"closest node of start node is  "<<start_node->x<<" , "<<start_node->y<<std::endl;
    std::cout<<"closest node of end node is "<<end_node->x<<" , "<<end_node->y<<std::endl;
}

std::vector<RouteModel::Node> RoutePlanner::constructFinalPath(RouteModel::Node* current_node){

    std::vector<RouteModel::Node> path_found ={};
    distance=0.0f;
    RouteModel::Node parent;
    while(current_node->parent!=nullptr){
        path_found.push_back(*current_node);
        parent=*current_node->parent;
        distance+=current_node->distance(&parent);
        current_node=&parent;
    } 
    path_found.push_back(*current_node);
    distance*=m_Model.MetricScale();
    return path_found;
}

void RoutePlanner::AStarSearch(){
    start_node->visited = true ;
    open_list.push_back(start_node);
    RouteModel::Node* current_node = nullptr;
    //std::cout<<"still alive"<<std::endl;
    
    while(open_list.size() > 0){

        /* std::cout<<"Openlist before : "<<std::endl;
        for(RouteModel::Node* n :open_list){
            std::cout<<" x "<<n->x<<" y "<<n->y<<std::endl;

        } */
        current_node = NextNode();
       /*   std::cout<<"current node ->x "<<current_node->x<<" current node->y "<<current_node->y<<std::endl; */
        //print openlist 
      /*   std::cout<<"Openlist after : "<<std::endl;
        for(RouteModel::Node* n :open_list){
            std::cout<<" x "<<n->x<<" y "<<n->y<<std::endl;

        }
        */

        if (current_node->distance(this->end_node) == 0){
            std::cout<<"found the path "<<std::endl;
            m_Model.path = constructFinalPath(current_node);
            return;
        }
        else{
            AddNeighbors(current_node);
        }

    }

}

float RoutePlanner::CalculateHValue(const RouteModel::Node* node){
    return node->distance(end_node);
}

RouteModel::Node* RoutePlanner::NextNode(){
    std::sort(open_list.begin(),open_list.end(),[](const auto &_1st,const auto &_2nd){
        return ((_1st->h_value+_1st->g_value) < (_2nd->h_value+_2nd->g_value)) ;
    });
    //printing sorted openlist
       std::cout<<"Openlist after : "<<std::endl;
        for(RouteModel::Node* n :open_list){
            std::cout<<"index : "<<n->GetIndex()<<"------- x "<<n->x<<" y "<<n->y<<" f: " << n->g_value+n->h_value<<" visited : "<<n->visited<<std::endl;

        }

    RouteModel::Node* next_node = open_list.front();
      std::cout<<"current node ::"<<"index : "<<next_node->GetIndex()<<" x-> "<<next_node->x<<" current node->y "<<next_node->y<<std::endl;
      std::cout<<"the erased node is "<<"index : "<<next_node->GetIndex()<<" x-> "<<open_list.front()->x<<" "<<open_list.front()->y<<std::endl;
    open_list.erase(open_list.begin());
   // std::cout<<"the erased node is "<<open_list.front()->x<<" "<<open_list.front()->y<<std::endl;
    return next_node;    


}


void RoutePlanner::AddNeighbors(RouteModel::Node* currnet_node){

    open_list={};
    currnet_node->FindNeighbors();
    //std::cout<<__func__<<std::endl;

    for(RouteModel::Node* neighbor : currnet_node->neighbors ){
        neighbor->parent = currnet_node ;
        neighbor->g_value = currnet_node->g_value + currnet_node->distance(neighbor);
        neighbor->h_value = CalculateHValue(neighbor);
        neighbor->visited =true ;
        open_list.push_back(neighbor);

    }
      //printing openlist after adding neighbors
       std::cout<<"Openlist after adding neighbors: "<<std::endl;
        for(RouteModel::Node* n :open_list){
            std::cout<<"index : "<<n->GetIndex()<<"------ x "<<n->x<<" y "<<n->y<<" f: " << n->g_value+n->h_value<<" visited : "<<n->visited<<std::endl;

        }

}