#include "route_model.h"
#include <iostream>

RouteModel::RouteModel(const std::vector<std::byte> &xml) : Model(xml) {
    int counter=0;
    for(Model::Node n : this->Nodes()){
        this->m_nodes.push_back(Node(counter,this,n));
        counter++;
    }

    RouteModel::createNodeToRoadHashmap();
}
float RouteModel::Node::distance (Node* node)const noexcept{

    return sqrt(pow(this->x-node->x,2)+pow(this->y-node->y,2));
}

void RouteModel::createNodeToRoadHashmap(){
    for(const Model::Road &road : Roads()){
        if(road.type != Model::Road::Type::Footway){
                for(int node_idx : this->Ways()[road.way].nodes){
                     if(this->node_to_road.find(node_idx) == node_to_road.end()){
                         node_to_road[node_idx] = std::vector<const Model::Road* > ();
                     }
                     node_to_road[node_idx].emplace_back(&road);
                    // std::cout<<"node_index in the hash table "<<node_idx<<std::endl;
                    // std::cout<<"size of this road noads "<<node_to_road[node_idx].size()<<std::endl;
                }
            }

    }
}

RouteModel::Node* RouteModel::Node::FindNeighbor(std::vector<int> node_indices){
    //std::cout<<__func__<<std::endl;
    Node* closest_node = nullptr;
    Node* node;
    //std::vector<RouteModel::Node*> tempo_nodes=this->parent_model->getNodes();
    for(int node_index :node_indices){
        node = &(this->parent_model->getNodes()[node_index]);
      //  std::cout<<"potential closest node x and y are : "<<node.x<<" "<<node.y<<std::endl;
      std::cout<<"checking node :"<<node->index<<" visited "<<node->visited<<std::endl;
        if ((this->distance(node) != 0.0) && ( (node->visited)!=true) )
        {
            if (closest_node==nullptr || (this->distance(node) < this->distance(closest_node))){
                closest_node = node;
            }
        }
    }
    //std::cout<<"Closest node index is "<<closest_node->index<<" with x "<<closest_node->x<<" and y "<<closest_node->y<<std::endl;
    //std::cout<<"address of closest node "<<closest_node<<std::endl;
    return closest_node;
}

void RouteModel::Node::FindNeighbors(){
    //std::cout<<__func__<<std::endl;
    //std::cout<<"index of the node is "<<this->index<<std::endl;
    RouteModel::Node* TheNeighbor = nullptr;
    for(auto &road : this->parent_model->node_to_road[this->index]){
        TheNeighbor= FindNeighbor(parent_model->Ways()[road->way].nodes);
         //std::cout<<"neighbor node index is "<<(*TheNeighbor).index<<" with x "<<(*TheNeighbor).x<<" and y "<<(*TheNeighbor).y<<std::endl;
           // std::cout<<"address of the neighbor is "<<TheNeighbor<<std::endl;
            if (TheNeighbor){
                this->neighbors.push_back(TheNeighbor);
                //std::cout<<"neighbor x and y are "<<TheNeighbor->x<<" "<<TheNeighbor->y<<std::endl;
            }     
    }
}

RouteModel::Node &RouteModel::FindClosestNode(float x,float y){

    Node input;
     input.x = x;
     input.y = y;
    float min_dist = std::numeric_limits<float>::max();
    int closest_idx;
    float dist;
    for(Road road:Roads()){
        if(road.type != Model::Road::Type::Footway){
            for(int node_idx : this->Ways()[road.way].nodes){
                dist = input.distance(&(getNodes()[node_idx]));
                if (dist < min_dist){
                    closest_idx=node_idx;
                    min_dist=dist;
                }
            }
        }

    }
    return getNodes()[closest_idx];
}

int RouteModel::Node::GetIndex(){
    return this->index;

}